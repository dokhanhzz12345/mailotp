package com.example.demootp.repository;

import com.example.demootp.bean.Email;
import com.example.demootp.bean.EmailRowMapper;
import com.example.demootp.utils.GenerateOTP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.stereotype.Repository;
import org.apache.commons.lang3.time.DateUtils;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

@Repository("emailRepository")
public class EmailRepositoryImpl implements EmailRepository{
    @Autowired
    JdbcTemplate jdbcTemplate;
    Email em = new Email();
    String otp = em.getOtp();
    @Override
    public boolean requestOTP(Email e) {
        String sql = "INSERT INTO otp(`id`, `email`, `otp`, `created`, `expries`) VALUES (?,?,?,?,?)";

        return jdbcTemplate.execute(sql, new PreparedStatementCallback<Boolean>() {
            @Override
            public Boolean doInPreparedStatement(PreparedStatement pst) throws SQLException, DataAccessException {
                pst.setInt(1, e.getId());
                pst.setString(2, e.getEmail());
                pst.setString(3, otp);
                pst.setString(4, e.getDateCreated());
                pst.setString(5, e.getDateExpries());
                return pst.execute();
            }
        });
    }

    @Override
    public void sendEmail(String mail) throws AddressException, MessagingException, IOException, AddressException{
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("dokhanhzz12345@gmail.com", "103@khanh");
            }
        });
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(mail, false));

        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail));
        msg.setSubject("Test");
        msg.setContent("Tutorials point email", "text/html");
        msg.setSentDate(new Date());

        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(otp, "text/html");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        msg.setContent(multipart);
        Transport.send(msg);
    }

    @Override
    public boolean checkLogin(Email email) {
       String sql="SELECT * FROM otp WHERE email = ? and otp= ?";
        List<Email> result = jdbcTemplate.query(sql,new Object[]{email.getEmail(),email.getOtp()}, new EmailRowMapper());
        if(result.size()>0){
            return true;
        }
        return false;
    }

    @Override
        public int Authenticate(Email e) {
        String sql = "UPDATE otp SET otp=null, confirm= ? WHERE otp=? AND email=?";
        Object[] params ={e.getConfirm(),e.getOtp(), e.getEmail()};
        int[] types ={Types.TIMESTAMP, Types.INTEGER,Types.VARCHAR};
        return jdbcTemplate.update(sql,params,types);
    }

    @Override
    public Email findByCreateTime(String DateCreated) {
        String sql ="SELECT * FROM otp WHERE created=?";
        try{
            return (Email) this.jdbcTemplate.queryForObject(sql, new Object[]{DateCreated}, new EmailRowMapper());

        }catch(EmptyResultDataAccessException ex){
            return null;
        }
    }

    @Override
    public Email findById(int id) {
        String sql ="SELECT * FROM otp WHERE id=?";
        try{
            return (Email) this.jdbcTemplate.queryForObject(sql, new Object[]{id}, new EmailRowMapper());

        }catch(EmptyResultDataAccessException ex){
            return null;
        }
    }

    @Override
    public Email findByOTP(String otp) {
        String sql ="SELECT * FROM otp WHERE otp=?";
        try{
            return (Email) this.jdbcTemplate.queryForObject(sql, new Object[]{otp}, new EmailRowMapper());

        }catch(EmptyResultDataAccessException ex){
            return null;
        }
    }


    @Override
    public void clearOTP() {

    }


}
