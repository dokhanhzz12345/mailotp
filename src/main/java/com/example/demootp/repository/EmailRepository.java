package com.example.demootp.repository;

import com.example.demootp.bean.Email;
import org.springframework.stereotype.Repository;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import java.io.IOException;
import java.sql.Timestamp;

@Repository
public interface EmailRepository {
    boolean requestOTP(Email e);

    //    public void sendEmail(String email, String otp) throws AddressException, MessagingException, IOException;
    void sendEmail(String email) throws MessagingException, IOException;

    //    public int Authenticate(Email e);
//    int Authenticate(String onetime);
    boolean checkLogin(Email email);
    public int Authenticate(Email e);
    public Email findByCreateTime(String DateCreated);
    public Email findById(int id);
    public Email findByOTP(String otp);
    void clearOTP();
}
