package com.example.demootp.service;

import com.example.demootp.bean.Email;
import com.example.demootp.repository.EmailRepository;
import com.example.demootp.utils.GenerateOTP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Properties;

@Service
public class EmailServiceImpl implements EmailService{
    @Autowired
    EmailRepository emailRepository;
    @Override
    public boolean requestOTP(Email e) {
       return emailRepository.requestOTP(e);
    }

    @Override
    public void sendEmail(String email) throws AddressException, MessagingException, IOException {
        emailRepository.sendEmail(email);
    }

    @Override
    public boolean checkLogin(Email email) {
        return emailRepository.checkLogin(email);
    }

    @Override
    public int Authenticate(Email e) {
        return emailRepository.Authenticate(e);
    }

    @Override
    public Email findByCreateTime(String DateCreated) {
        return emailRepository.findByCreateTime(DateCreated);
    }

    @Override
    public Email findById(int id) {
        return emailRepository.findById(id);
    }

    @Override
    public Email findByOTP(String otp) {
        return emailRepository.findByOTP(otp);
    }

}
