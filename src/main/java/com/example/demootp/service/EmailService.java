package com.example.demootp.service;

import com.example.demootp.bean.Email;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import java.io.IOException;
import java.sql.Timestamp;

@Service
public interface EmailService {
    boolean requestOTP(Email e);
    void sendEmail(String email) throws MessagingException, IOException;
    boolean checkLogin(Email email);
    public int Authenticate(Email e);
    public Email findByCreateTime(String DateCreated);
    public Email findById(int id);
    public Email findByOTP(String otp);
}
