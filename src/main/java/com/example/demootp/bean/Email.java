package com.example.demootp.bean;


import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.example.demootp.utils.GenerateOTP;
import lombok.*;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Email {
    GenerateOTP generateOTP = new GenerateOTP();
    int id;
    String Otp = new String(generateOTP.OTP(8));
    String email;
    Date confirm= new Date();
    Date created= new Date();
    Date expries = DateUtils.addMinutes(created,1);
    String DateConfirm= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(confirm);
    String DateCreated= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(created);
    String DateExpries= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(expries);


//    public Date getExpires() {
//        return expires;
//    }
//
//    public Date getConfirm() {
//        return confirm;
//    }
//
//    public Date getCreated() {
//        return created;
//    }

}
