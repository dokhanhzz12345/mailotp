package com.example.demootp.bean;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

// java.sql.Date sqlDateConfirm = new java.sql.Date(confirm.getTime());
//    java.sql.Date sqlDateCreated = new java.sql.Date(created.getTime());
//    java.sql.Date sqlDateExpires = new java.sql.Date(expires.getTime());
public class EmailRowMapper implements RowMapper<Email> {
    @Override
    public Email mapRow(ResultSet rs, int i) throws SQLException {
        Email e = new Email();
        e.setId(rs.getInt("id"));
        e.setOtp(rs.getString("otp"));
        e.setEmail(rs.getString("email"));
        e.setDateCreated(rs.getString("created"));
        e.setDateExpries(rs.getString("expries"));
        e.setDateConfirm(rs.getString("confirm"));
        return e;
    }
}
