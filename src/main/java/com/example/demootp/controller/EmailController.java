package com.example.demootp.controller;

import com.example.demootp.bean.Email;
import com.example.demootp.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.mail.MessagingException;
import java.io.IOException;

@Controller
@SessionAttributes({"loginInfo", "notice" })
public class EmailController {
    @Autowired
    EmailService emailService;
    @Autowired
    Email em;

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/index")
    public String index(Model model) {
        model.addAttribute("mail", new Email());
        return "MailRequest";
    }

    @RequestMapping(value = "/requestOTP", method = RequestMethod.POST)
    public String save(@ModelAttribute("mail") Email mail, @RequestParam(name = "email", required = false, defaultValue = "")
            String email, Model model) throws MessagingException, IOException {
//        model.addAttribute("email", email);
        emailService.requestOTP(mail);
        emailService.sendEmail(mail.getEmail());
        Email ma = emailService.findByCreateTime(mail.getDateCreated());
//        model.addAttribute("thu",ma);
        System.out.println("Thời gian tạo: "+mail.getDateCreated());
        return "redirect:/login";
    }

    @RequestMapping(value = "/login")
    public String login(ModelMap model, @ModelAttribute("thu") Email mail) {
        System.out.println("Thời gian tạo khi login: "+mail.getDateCreated());
        return "Authenticate";
    }

    @RequestMapping("/authen")
    public String authen(@ModelAttribute("thu") Email mail, Model model) throws MessagingException, IOException, ParseException {
        if (emailService.checkLogin(mail)) {
            Email e = emailService.findByOTP(mail.getOtp());
            emailService.Authenticate(mail);
            Email em = emailService.findByCreateTime(e.getDateCreated());
            if (em.getDateExpries().compareTo(em.getDateConfirm()) < 0){
                model.addAttribute("loginInfo", em.getEmail());
                model.addAttribute("notice", "Mã OTP đã hết hạn, hãy yêu cầu mã mới");
            }
            else if (em.getDateExpries().compareTo(em.getDateConfirm()) > 0) {
                System.out.println("Con thoi han");
                model.addAttribute("loginInfo", em.getEmail());
                model.addAttribute("notice", "Bạn đã xác thực thành công");
            }
            return "notification";
        }
        return "redirect:/login";
    }
    @RequestMapping("/back")
    public String back(ModelMap model){
        model.addAttribute("notice","");
        model.addAttribute("loginInfo","");
        return "redirect:/index";
    }

}